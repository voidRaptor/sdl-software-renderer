OBJS =  src/*.cc \
		src/*/*.cc

CC = g++

COMPILER_FLAGS = -Wall -std=c++17

LINKER_FLAGS = -lSDL2 -lSDL2_ttf -lSDL2_image

OBJ_NAME = swr.out
DB_NAME = debug

.PHONY: all debug

all : $(OBJS) ;	$(CC) $(COMPILER_FLAGS) $(LINKER_FLAGS) $(OBJS) -o $(OBJ_NAME)

debug : $(OBJS) ; $(CC) $(COMPILER_FLAGS) -g $(LINKER_FLAGS) $(OBJS) -o $(DB_NAME)