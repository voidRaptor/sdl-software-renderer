#ifndef MODEL_HH
#define MODEL_HH

#include "camera.hh"
#include "mesh.hh"

#include <GL/glew.h>
#include <glm/ext.hpp>
#include <glm/glm.hpp>
#include <vector>

using MeshVec = std::vector<Mesh*>;

// TODO: mesh owns vertices -> invent efficient way to sotre model specific vertices

class Model {

public:

    Model(Vertex* vertices, unsigned int vertexCount, 
          unsigned int* indices, unsigned int indexCount,
          const glm::vec3 &pos=glm::vec3(),
          const glm::vec3 &rot=glm::vec3(),
          const glm::vec3 &scale=glm::vec3(1.0f, 1.0f, 1.0f),
          const unsigned int tex=0);

    virtual ~Model();

    virtual void update(Camera* cam);

    glm::mat4 getModel() const;
    glm::vec3 getPos() const;
    glm::vec3 getRot() const;
    glm::vec3 getScale() const;
    MeshVec getMeshes() const;
//    Material getMaterial();

    void setPos(glm::vec3 &newPos);
    void setRot(glm::vec3 &newRot);
    void setScale(glm::vec3 &newScale);




protected:

    MeshVec mMeshes;
    glm::vec3 mPos;
    glm::vec3 mRot;
    glm::vec3 mScale;

    glm::vec3 mOrigPos;
//    Material mMaterial;


};

#endif
