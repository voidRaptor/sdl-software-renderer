#include "bitmap.hh"
#include "window.hh"

#include <iostream>

Bitmap::Bitmap(int x, int y, int w, int h, unsigned int* pixels):
    mDim( {x, y, w, h} ),
    mPixels(pixels) {

        if (mPixels == nullptr) {
            mPixels = new unsigned int[mDim.w * mDim.h];
        }
        memset((void*)mPixels, 0x0, mDim.w * mDim.h *4);

}


Bitmap::~Bitmap() {

}


void Bitmap::render() {
    SDL_Surface* screen = Window::getScreen();
    unsigned int* sPixels = (unsigned int*)screen->pixels;

    // bitmap completely out of view, TODO: max is 1 pixel off?
    if (mDim.x + mDim.w < 0 || mDim.x >= screen->w || 
        mDim.y + mDim.h < 0 || mDim.y >= screen->h) {

        return;
    }


    for (int j=0; j<mDim.h; ++j) {
        for (int i=0; i<mDim.w; ++i) {

            // translate to correct coordinates -> use offsets
            int x = mDim.x + i;
            int y = mDim.y + j;

            // pixel out of view
            if (x < 0 || x > screen->w || 
                y < 0 || y > screen->h) {

                continue;
            }

            sPixels[x + y*screen->w] = mPixels[i + j*mDim.w];

        }

    }

}


void Bitmap::clear() {


}

