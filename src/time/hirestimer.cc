#include "hirestimer.hh"
#include <iostream>

HiResTimer::HiResTimer():
    ITimer() {
    
}


HiResTimer::~HiResTimer() {
    
}


double HiResTimer::getTotal() {
    return ( std::chrono::high_resolution_clock::now() - mTotalStart ).count();
}


double HiResTimer::getDelta() {
    auto current = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> delta = current - mDeltaStart;
    mDeltaStart = current;

    return delta.count();
    
    
/*
    auto t_lastFrame = std::chrono::high_resolution_clock::now();
    std::chrono::time_point<std::chrono::high_resolution_clock> t_currentFrame; 
    std::chrono::duration<double> t_delta;

    t_currentFrame = std::chrono::high_resolution_clock::now();
    t_delta = t_currentFrame - t_lastFrame;
    t_lastFrame = t_currentFrame;

*/
}


void HiResTimer::start() {
    mTotalStart = std::chrono::high_resolution_clock::now();  
    mDeltaStart = std::chrono::high_resolution_clock::now();      
}

    