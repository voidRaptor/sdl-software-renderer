#include "lowrestimer.hh"

LowResTimer::LowResTimer():
    ITimer() {
    
}


LowResTimer::~LowResTimer() {
    
}


double LowResTimer::getTotal() {
    return (SDL_GetTicks() - mTotalStart)*0.001f;
}


double LowResTimer::getDelta() {
    unsigned int current = SDL_GetTicks();
    unsigned int delta = current - mDeltaStart;
    mDeltaStart = current;
    
    return delta * 0.001f;
}


void LowResTimer::start() {
    mTotalStart = SDL_GetTicks();
    mDeltaStart = SDL_GetTicks();
}

