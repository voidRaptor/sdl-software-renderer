#include "stopwatch.hh"
#include "hirestimer.hh"
#include "lowrestimer.hh"


Stopwatch::Stopwatch(Mode mode, double timeout):
    mTimer(nullptr),
    mTimeout(timeout) {
        
    if (mode == Mode::HiRes) mTimer = new HiResTimer;
    else if (mode == Mode::LowRes) mTimer = new LowResTimer;    
}


Stopwatch::~Stopwatch() {
    delete mTimer;
}


void Stopwatch::reset() {
    start(mTimeout);  // ensure same timeout is used on reset
}


void Stopwatch::start(double timeout) {
    mTimeout = timeout;
    mTimer->start();
}


double Stopwatch::getTotalTime() {
    return mTimer->getTotal();
}


double Stopwatch::getDeltaTime() {
    return mTimer->getDelta();
}


bool Stopwatch::isTimeout() {
    
    if (mTimeout > -1.0f) return (getTotalTime() > mTimeout);
    return false; 
} 

