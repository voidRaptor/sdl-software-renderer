#ifndef LOWRESTIMER_HH
#define LOWRESTIMER_HH

#include "itimer.hh"
#include <SDL2/SDL.h>


/**
 * @brief LowResTimer, timer with less precision, counts milliseconds as integers
 */
class LowResTimer: public ITimer {

public:

    LowResTimer();
    virtual ~LowResTimer();
    
    virtual double getTotal();
    virtual double getDelta();
    
    virtual void start();
    
    
private:
    double mTotalStart;
    double mDeltaStart;
    
};


#endif