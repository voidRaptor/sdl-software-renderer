#include "window.hh"
#include "renderer.hh"

#include <string>
#include <iostream>



const int Window::W = 800;
const int Window::H = 600;
const char* Window::TITLE = "SWR";

const SDL_Color Window::BG_COLOR = {100, 0, 255, 255};
SDL_Surface* Window::mScreen = nullptr;

Window::Window():
    mInited(false),
    mWindow(nullptr) {
    
        mInited = init();
    
}


Window::~Window() {
    
}


bool Window::init() {
    
    SDL_Init(SDL_INIT_EVERYTHING);
    
    mWindow = SDL_CreateWindow(TITLE,
                               SDL_WINDOWPOS_CENTERED,
                               SDL_WINDOWPOS_CENTERED,
                               W,
                               H,
                               0);


    if (mWindow == nullptr) {
        std::cerr << "Window: " << SDL_GetError() << "\n";
        return false;
    }

    mScreen = SDL_GetWindowSurface(mWindow);
    
    if (mWindow == nullptr) {
        std::cerr << "Window: " << SDL_GetError() << "\n";
        return false;
    }

    // SDL_PixelFormat format = {SDL_PIXELFORMAT_RGBX8888, NULL, 32, 4};
    // SDL_ConvertSurface(mScreen, &format, 0);

    return true;
    
}


bool Window::isInited() const {
    return mInited;
}


SDL_Surface* Window::getScreen() {
    return mScreen;
}


unsigned int Window::getPixelCount() const {
    return mScreen->w * mScreen->h;
}


void Window::swapBuffers() {
    SDL_UpdateWindowSurface(mWindow);
}


void Window::clear() {
    // NOTE: memset set per byte: 1 pixel = 4B -> 4 * pixelcount
    // memset(mScreen->pixels, 0xFF, mScreen->w * mScreen->h * sizeof(unsigned int));

    Renderer::clear((unsigned int*)mScreen->pixels, mScreen->w * mScreen->h, BG_COLOR);
}


void Window::updateFPS(float &deltaTime) {
    std::string title = std::string(TITLE) + " FPS: " + std::to_string(int((1/deltaTime)));
    SDL_SetWindowTitle(mWindow, title.c_str());
}

