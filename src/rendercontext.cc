#include "rendercontext.hh"
#include "renderer.hh"


RenderContext::RenderContext(int x, int y, int w, int h):
    Bitmap(x, y, w, h) {

        mScanBuffer = new unsigned int[h*2];
}


RenderContext::~RenderContext() {

}


void RenderContext::drawTriangle(const Vertex &a, 
                                 const Vertex &b, 
                                 const Vertex &c) {

    // TODO: unnecessary? benny does this though
    // BUG(?): transforms every x into same position
    glm::mat4 m = getSSTransform(mDim.w/2, mDim.h/2);
    Vertex min = Vertex(a.transform(m));
    Vertex mid = Vertex(b.transform(m));
    Vertex max = Vertex(c.transform(m));

    // Vertex min = a;
    // Vertex mid = b;
    // Vertex max = c;

    std::cout << min.pos.x << ", " << min.pos.y << ", " << min.pos.z << ", " << min.pos.w << "\n";
    std::cout << mid.pos.x << ", " << mid.pos.y << ", " << mid.pos.z << ", " << mid.pos.w << "\n";
    std::cout << max.pos.x << ", " << max.pos.y << ", " << max.pos.z << ", " << max.pos.w << "\n";

    Vertex::perspectiveDiv(min);
    Vertex::perspectiveDiv(mid);
    Vertex::perspectiveDiv(max);

    swapSort(min, mid, max);

    glm::vec2 u = {max.pos.x - min.pos.x, max.pos.y - min.pos.y};
    glm::vec2 v = {mid.pos.x - min.pos.x, mid.pos.y - min.pos.y};
    float area = 0.5f * glm::determinant( glm::mat2(u, v) );
    
    int rhs = 0;
    if (area >= 0.0f) rhs = 1;

    drawScanLine(min, max, rhs);
    drawScanLine(min, mid, 1 - rhs);
    drawScanLine(mid, max, 1 - rhs);


    fillTriangle(min.pos.y, max.pos.y);

    render();

}


void RenderContext::setScanLimits(int y, int xMin, int xMax) {
    mScanBuffer[y*2]     = xMin;
    mScanBuffer[y*2 + 1] = xMax; 
}


void RenderContext::drawScanLine(const Vertex &min, 
                                 const Vertex &max, 
                                 int rhs) {

    int xDist = max.pos.x - min.pos.x;
    int yDist = max.pos.y - min.pos.y;

    if (yDist <= 0) return;

    float xStep = (float)xDist / (float)yDist;
    float xCurrent = min.pos.x;

    for (int j=min.pos.y; j<max.pos.y; ++j) {
        mScanBuffer[j*2 + rhs] = (unsigned int)xCurrent;
        xCurrent += xStep;
    }

}


void RenderContext::fillTriangle(int yMin, int yMax) {

    for (int j=yMin; j<yMax; ++j) {

        int xMin = mScanBuffer[j*2];
        int xMax = mScanBuffer[j*2 + 1];

        for (int i=xMin; i<xMax; ++i) {

            // out of view
            if (i < 0 || i > mDim.w || j < 0 || j > mDim.h) {
                continue;
            }
            Renderer::drawPixel(mPixels, mDim.w, i, j, 0xFF0000);
        }

    }

}


void RenderContext::swapSort(Vertex &min, Vertex &mid, Vertex &max) {

    if (mid.pos.y > max.pos.y) {
        Vertex tmp = max;
        max = mid;
        mid = tmp;
    }

    if (min.pos.y > mid.pos.y) {
        Vertex tmp = mid;
        mid = min;
        min = tmp;
    }


    if (mid.pos.y > max.pos.y) {
        Vertex tmp = max;
        max = mid;
        mid = tmp;
    }

}


glm::mat4 RenderContext::getSSTransform(float halfW, float halfH) {

    glm::mat4 trans;
    trans[0][0] = halfW;  trans[0][1] = 0;       trans[0][2] = 0;      trans[0][3] = halfW;
    trans[1][0] = 0;      trans[1][1] = -halfH;  trans[1][2] = 0;      trans[1][3] = halfH;
    trans[2][0] = 0;      trans[2][1] = 0;       trans[2][2] = 1;      trans[2][3] = 0;
    trans[3][0] = 0;      trans[3][1] = 0;       trans[3][2] = 0;      trans[3][3] = 1;
 
    return trans;
}