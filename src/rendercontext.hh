#ifndef RENDERCONTEXT_HH
#define RENDERCONTEXT_HH

#include "bitmap.hh"
#include "vertex.hh"

#include <glm/glm.hpp>


class RenderContext: public Bitmap {

public:
    RenderContext(int x, int y, int w, int h);
    ~RenderContext();

    void drawTriangle(const Vertex &a, 
                      const Vertex &b, 
                      const Vertex &c);

private:

    void setScanLimits(int y, int xMin, int xMax);



    void drawScanLine(const Vertex &min, 
                      const Vertex &max, 
                      int rhs);

    void fillTriangle(int yMin, int yMax);

    void swapSort(Vertex &min, Vertex &mid, Vertex &max);
    

    // TODO: find better location
    glm::mat4 getSSTransform(float halfW, float halfH);

    unsigned int* mScanBuffer;


};


#endif