#include "control.hh"


int main(int argc, char** argv) {

    Control c;
    if ( !c.isInited() ) {
        return 1;
    }

    c.run();

    std::cout << "Exited normally\n";

    return 0;
}