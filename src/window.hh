#ifndef WINDOW_HH
#define WINDOW_HH

#include <SDL2/SDL.h>

/**
 * @brief Window, wrapper for SDL_Window render context
 */
class Window {

public:
    Window();
    ~Window();
    
    bool isInited() const;
    
    static SDL_Surface* getScreen();
    unsigned int getPixelCount() const;

    void swapBuffers();
    void clear();
    void updateFPS(float &deltaTime);
    
    static const int W;
    static const int H;
    static const char* TITLE;


private:
    bool init();

    bool mInited;
    SDL_Window* mWindow;
    static SDL_Surface* mScreen;

    // SDL_CreateSofwareRenderer?
    
    static const SDL_Color BG_COLOR; 

    
};



#endif