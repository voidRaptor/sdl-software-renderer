#include "camera.hh"
#include "window.hh"

const glm::vec3 Camera::POS = glm::vec3(0,0,-3);
const float Camera::FOV     = 70.0f;                          
const float Camera::ASPECT  = float(Window::W)/float(Window::H);
const float Camera::Z_NEAR  = 0.1f;                          
const float Camera::Z_FAR   = 1000.0f;                       
const float Camera::SPEED   = 2.0f;

Camera* Camera::getInstance() {
    static Camera instance;
    return &instance;
}


Camera::Camera():

    mPerspective(glm::perspective(FOV, ASPECT, Z_NEAR, Z_FAR)),
    mCoord( {POS, glm::vec3(0,0,1), glm::vec3(0,1,0)} ),
    mPitch(0.0f),
    mYaw(90.0f),
    mLastX(Window::W/2.0f),
    mLastY(Window::H/2.0f) {
    
}


Camera::~Camera() {
}


glm::mat4 Camera::getViewProjection() {
    return mPerspective * getView();
}


glm::mat4 Camera::getView() {
    return glm::lookAt(mCoord.pos, mCoord.pos + mCoord.front, mCoord.up);
}


glm::mat4 Camera::getProjection() {
    return mPerspective;
}


glm::vec3 Camera::getPos() {
    return mCoord.pos;
}


glm::vec3 Camera::getFront() const {
    return mCoord.front;
}


glm::vec3 Camera::getUp() const {
    return mCoord.up;
}


void Camera::updateView(float &deltaTime) {

    float camSpeed = SPEED * deltaTime;
    
    Input::getInstance()->handleMovement(&mAccel);

    // update camera movement
    mCoord.pos +=  mAccel.forward * camSpeed * mCoord.front;
    mCoord.pos +=  mAccel.strafe * glm::normalize( glm::cross(mCoord.front, mCoord.up) ) * camSpeed;
    mCoord.pos.y += mAccel.ascend * camSpeed;

    // update camera rotation
    mPitch += mAccel.pitch;
    mYaw += mAccel.yaw;

    // prevents camera going upside down
    if (mPitch > 89.0f) {
        mPitch = 89.0f;
    }

    if (mPitch < -89.0f) {
        mPitch = -89.0f;
    }

    // update view
    mCoord.front.x = cos(glm::radians(mPitch)) * cos(glm::radians(mYaw));
    mCoord.front.y = sin(glm::radians(mPitch));
    mCoord.front.z = cos(glm::radians(mPitch)) * sin(glm::radians(mYaw));

}


