#include "input.hh"


Input* Input::getInstance() {
    static Input instance;
    return &instance;
}


Input::Input() {
        
        
}


bool Input::shouldQuit() {
    while ( SDL_PollEvent(&mEvent) ) {
        
        if (mEvent.type == SDL_QUIT || mEvent.key.keysym.sym == SDLK_q ) {
            return true;
        }
        
    }
    return false;
}


void Input::handleMovement(Acceleration* accel) {
    
    SDL_PumpEvents();
    const Uint8* keyStates = SDL_GetKeyboardState(NULL);
    
    // position
    if ( keyStates[SDL_SCANCODE_W] )      accel->forward =  1.0f;
    else if ( keyStates[SDL_SCANCODE_S] ) accel->forward = -1.0f;
    else                                  accel->forward =  0.0f;
    
    if ( keyStates[SDL_SCANCODE_A] )      accel->strafe = -1.0f;
    else if ( keyStates[SDL_SCANCODE_D] ) accel->strafe =  1.0f; 
    else                                  accel->strafe =  0.0f;  
    
    // direction
    if ( keyStates[SDL_SCANCODE_I] )      accel->pitch =  1.0f;
    else if ( keyStates[SDL_SCANCODE_K] ) accel->pitch = -1.0f;
    else                                  accel->pitch =  0.0f;
    
    if ( keyStates[SDL_SCANCODE_J] )      accel->yaw = -1.0f;
    else if ( keyStates[SDL_SCANCODE_L] ) accel->yaw =  1.0f; 
    else                                  accel->yaw =  0.0f;
    
    // ascend
    if ( keyStates[SDL_SCANCODE_SPACE] )       accel->ascend =  1.0f;
    else if ( keyStates[SDL_SCANCODE_LSHIFT] ) accel->ascend = -1.0f;
    else                                       accel->ascend =  0.0f;

}

