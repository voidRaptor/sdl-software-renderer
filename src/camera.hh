#ifndef CAMERA_HH
#define CAMERA_HH

#include "input.hh"

#include <glm/ext.hpp>
#include <glm/glm.hpp>
#include <SDL2/SDL.h>


struct CamCoord {
    glm::vec3 pos;
    glm::vec3 front;
    glm::vec3 up;
};


class Camera {

public:
    
    static Camera* getInstance();
    
    glm::mat4 getViewProjection();
    glm::mat4 getView();
    glm::mat4 getProjection();

    glm::vec3 getPos();
    glm::vec3 getFront() const;
    glm::vec3 getUp() const;

    void updateView(float &deltaTime);

    Camera(const Camera&) = delete;
    Camera(Camera&&) = delete;
    Camera& operator=(const Camera&) = delete;
    Camera& operator=(Camera&&) = delete;
    
private:
    Camera();
    ~Camera();
    
    glm::mat4 mPerspective;
    CamCoord mCoord;
    
    Acceleration mAccel;

    // Euler angles
    float mPitch;
    float mYaw;

    float mLastX;
    float mLastY;

    static const glm::vec3 POS;  // camera start position
    static const float FOV;      // field of vision
    static const float ASPECT;   // viewport dimensions
    static const float Z_NEAR;   // distance to closer zy clip plane
    static const float Z_FAR;    // distance to farther zy clip plane
    static const float SPEED;


};


#endif
