#ifndef INPUT_HH
#define INPUT_HH

#include <iostream>
#include <SDL2/SDL.h>

struct Acceleration {

    float forward;      // towards or away from center of view
    float strafe;       // sideways
    float ascend;       // ascend (descend if negative)
    float pitch;        // camera up and down rotation
    float yaw;          // camera left and right rotation

};

/**
 * @brief Input, singleton for handling all input
 * 
 */
class Input {

public:
    static Input* getInstance();
    
    /**
     * @brief shouldQuit, used to detect exit commands and close main update loop
     * @param e, SDL eventhandler item 
     */
    bool shouldQuit();
    
    
    /**
     * @brief handleMovement, register all key presses related to camera movement
     * @param accel, struct of camera acceleration for different directions  
     */
    void handleMovement(Acceleration* accel);
    
    
    Input(const Input&) = delete;
    Input(Input&&) = delete;
    Input& operator=(const Input&) = delete;
    Input& operator=(Input&&) = delete;
    

private:
    Input();
    ~Input() {};
    
    SDL_Event mEvent;
    

};


#endif