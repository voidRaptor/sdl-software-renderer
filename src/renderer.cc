#include "renderer.hh"




void Renderer::drawPixel(unsigned int* pixels, 
                         unsigned int w,
                         unsigned int x, 
                         unsigned int y, 
                         unsigned int color) {

    pixels[x + y * w] = color; 
}



void Renderer::clear(unsigned int* pixels,
                     unsigned int count, 
                     unsigned int color) {

    for (unsigned int i=0; i<count; ++i) {
        pixels[i] = color; 
    }
}


void Renderer::clear(unsigned int* pixels,
                     unsigned int count, 
                     const SDL_Color &color) {

    for (unsigned int i=0; i<count; ++i) {
        pixels[i] = (color.r << 16) | (color.r << 8) | color.b; 
    }
}