#include "cube.hh"



Vertex Cube::VERTICES[] = {
    
    // front
    Vertex{ {-0.5f, -0.5f,  0.5f, 1} },
    Vertex{ { 0.5f, -0.5f,  0.5f, 1} },
    Vertex{ { 0.5f,  0.5f,  0.5f, 1} },
    Vertex{ {-0.5f,  0.5f,  0.5f, 1} },
    
    // back
    Vertex{ {-0.5f, -0.5f, -0.5f, 1} },
    Vertex{ { 0.5f, -0.5f, -0.5f, 1} },
    Vertex{ { 0.5f,  0.5f, -0.5f, 1} },
    Vertex{ {-0.5f,  0.5f, -0.5f, 1} },
    
};


// anti-clockwise
unsigned int Cube::INDICES[] =  {
    
    // front
    0, 1, 2,
    0, 2, 3,
    
    // back
    4, 5, 6,
    4, 6, 7,
    
    // left
    4, 0, 3, 
    4, 3, 7, 
    
    // right
    5, 1, 2, 
    5, 2, 6,
    
    // bottom
    0, 1, 5,
    0, 5, 4,
    
    // top
    3, 2, 6, 
    3, 6, 7
    
};


Vertex* NORMALS = {};


Cube::Cube():
    Model(VERTICES, 8, INDICES, 36) {
}


Cube::~Cube() {
    
}

