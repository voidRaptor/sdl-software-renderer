#include "model.hh"
#include <iostream>
#include <SDL2/SDL.h>


Model::Model(Vertex* vertices, 
             unsigned int vertexCount, 
             unsigned int* indices, 
             unsigned int indexCount,
             const glm::vec3 &pos,
             const glm::vec3 &rot,
             const glm::vec3 &scale,
             const unsigned int tex):

    mMeshes{ new Mesh(vertices, vertexCount, indices, indexCount) },
    mPos(pos),
    mRot(rot),
    mScale(scale),
    mOrigPos(pos) {

}


Model::~Model() {

    for (unsigned int i=0; i<mMeshes.size(); ++i) {
        delete mMeshes[i];
    }

}


void Model::update(Camera* cam) {
    
    // shader->setPerspective(cam, getModel(), false);

    // render vertices
    for (unsigned int i=0; i<mMeshes.size(); ++i) {
        mMeshes[i]->render( cam, getModel() );
    }

}



glm::mat4 Model::getModel() const {
    glm::mat4 trans;

    // change position
    trans = glm::translate(trans, mPos);

    // change angle
    trans = glm::rotate(trans, mRot.x, glm::vec3(1, 0, 0));
    trans = glm::rotate(trans, mRot.y, glm::vec3(0, 1, 0));
    trans = glm::rotate(trans, mRot.z, glm::vec3(0, 0, 1));

    // change scaling
    trans = glm::scale(trans, mScale);

    return trans;
}


glm::vec3 Model::getPos() const { return mPos; }
glm::vec3 Model::getRot() const { return mRot; }
glm::vec3 Model::getScale() const { return mScale; }
MeshVec Model::getMeshes() const { return mMeshes; }
//Material Model::getMaterial() { return mMaterial; }


void Model::setPos(glm::vec3 &newPos) { mPos = newPos; }
void Model::setRot(glm::vec3 &newRot) { mRot = newRot; }
void Model::setScale(glm::vec3 &newScale) { mScale = newScale; }



