#ifndef MESH_HH
#define MESH_HH

#include "camera.hh"
#include "rendercontext.hh"
#include "vertex.hh"

#include <glm/glm.hpp>
#include <iostream>
#include <string>
#include <vector>


class Mesh {

public:

    Mesh(Vertex* vertices, unsigned int vertexCount, 
         unsigned int* indices, unsigned int indexCount);

    Mesh(glm::vec2* vertices, unsigned int vertexCount, 
         unsigned int* indices, unsigned int indexCount);

    ~Mesh();

    void render(Camera* cam, const glm::mat4 &model);


private:

    void init();

    Vertex* mVertices;
    glm::vec2* mVertices2;
    unsigned int mVertexCount;

    unsigned int* mIndices;
    unsigned int mIndexCount;

    RenderContext* mContext;


};

#endif
