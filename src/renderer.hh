#ifndef RENDERER_HH
#define RENDERER_HH

#include <glm/glm.hpp>
#include <SDL2/SDL.h>

/**
 * @brief Renderer, collection of static drawing functions, forms low level renderig API
 * 
 */
class Renderer {

public:



    static void drawPixel(unsigned int* pixels,
                          unsigned int w, 
                          unsigned int x, 
                          unsigned int y, 
                          unsigned int color);

    static void clear(unsigned int* pixels,
                      unsigned int count, 
                      unsigned int color);

    static void clear(unsigned int* pixels,
                      unsigned int count, 
                      const SDL_Color &color);

};


#endif