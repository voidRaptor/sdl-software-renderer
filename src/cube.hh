#ifndef CUBE_HH
#define CUBE_HH

#include "model.hh"


class Cube: public Model {
    
public:
    Cube();
    ~Cube();
    

private:

    static Vertex VERTICES[];
    static unsigned int INDICES[];
    static glm::vec3 NORMALS[];

    
    
    
};



#endif