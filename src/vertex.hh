#ifndef VERTEX_HH
#define VERTEX_HH

#include <glm/glm.hpp>
#include <iostream>

/**
 * @brief Vertex, wrapper for storing single vertex
 * 
 * 
 */
class Vertex {

public:
    Vertex(const glm::vec4 &v):
        pos(v) {
    }

    Vertex():
        pos(0.0f) {
    }

    // Vertex(const Vertex &other):
    //     pos(other.pos) {
    // }

    // Vertex(Vertex &other):
    //     pos(other.pos) {
    // }
    

    glm::vec4 transform(const glm::mat4 &m) const {
/*
        std::cout << pos.x << ", " << pos.y << ", " << pos.z << ", " << pos.w << "\n\n";


        std::cout << m[0][0] << " " << m[0][1] << " " << m[0][2] << " " << m[0][3] << "\n"
                  << m[1][0] << " " << m[1][1] << " " << m[1][2] << " " << m[1][3] << "\n"
                  << m[2][0] << " " << m[2][1] << " " << m[2][2] << " " << m[2][3] << "\n"
                  << m[3][0] << " " << m[3][1] << " " << m[3][2] << " " << m[3][3] << "\n";
        return Vertex( {m * pos} );
*/
        glm::vec4 v;
        v.x = m[0][0] * pos.x + m[0][1] * pos.y + m[0][2] * pos.z + m[0][3] * pos.w;
        v.y = m[1][0] * pos.x + m[1][1] * pos.y + m[1][2] * pos.z + m[1][3] * pos.w;
        v.z = m[2][0] * pos.x + m[2][1] * pos.y + m[2][2] * pos.z + m[2][3] * pos.w;
        v.w = m[3][0] * pos.x + m[3][1] * pos.y + m[3][2] * pos.z + m[3][3] * pos.w;

        return v;
    }

    Vertex perspectiveDiv() {
        return Vertex( {pos.x/pos.w, pos.y/pos.w, pos.z/pos.w, pos.w} );
    }

    static void perspectiveDiv(Vertex &v) {
        v.pos.x /= v.pos.w; 
        v.pos.y /= v.pos.w; 
        v.pos.z /= v.pos.w; 
    }


    glm::vec4 pos;
};


#endif