#ifndef CONTROL_HH
#define CONTROL_HH

#include "camera.hh"
#include "input.hh"
#include "model.hh"
#include "time/stopwatch.hh"
#include "window.hh"

#include <string>
#include <unordered_map>

class Control {

public:
    Control();
    ~Control();
    
    void run();
    bool isInited() const;

private:
    bool init();    

    bool mInited;
    
    Camera* mCam;
    Input* mInput;
    Window mWindow;
    
};


#endif