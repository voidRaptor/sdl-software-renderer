#include "control.hh"
#include "cube.hh"
#include "bitmap.hh"
#include "renderer.hh"


Control::Control():
    mInited(false),
    mCam( Camera::getInstance() ),
    mInput( Input::getInstance() ) {
    
        mInited = init();
}


Control::~Control() {
    
}


void Control::run() {
    
    float deltaTime = 0.0f;
    Stopwatch frameTimer(Mode::HiRes);
    Stopwatch fpsUpdateTimer;

    frameTimer.start();
    fpsUpdateTimer.start(0.5f);

    Vertex v[] = {
        Vertex{ {100, 100,  0,  1} },
        Vertex{ {300, 500,  0,  1} },
        Vertex{ {500, 100,  0,  1} },
        // Vertex{ {4.0f, 1.0f, 0.0f, 1.0f} },
    };
    unsigned int i[] = {
        0, 1, 2,
        // 0, 2, 3
    };
    Mesh triangle(v, 3, i, 3);

    glm::mat4 trans;
    glm::vec3 rot{0.0f, 0.0f, 0.0f};
    trans = glm::translate(trans, glm::vec3{0.0f});
    trans = glm::rotate(trans, rot.x, glm::vec3(1, 0, 0));
    trans = glm::rotate(trans, rot.y, glm::vec3(0, 1, 0));
    trans = glm::rotate(trans, rot.z, glm::vec3(0, 0, 1));
    trans = glm::scale(trans, glm::vec3{1.0f});

    // update loop
    while ( !mInput->shouldQuit() ) {
        mWindow.clear();
        
        deltaTime = frameTimer.getDeltaTime();
        mCam->updateView(deltaTime);
        
        if ( fpsUpdateTimer.isTimeout() ) {
            mWindow.updateFPS(deltaTime);
            fpsUpdateTimer.reset();
        }

        // TODO: move attribute setting into Model?
        // cube.update(mCam);
        triangle.render(mCam, trans);
        
        
        mWindow.swapBuffers();
    }
    
}


bool Control::isInited() const {
    return mInited;
}


bool Control::init() {

    return mWindow.isInited();
}



