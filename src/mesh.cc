#include "mesh.hh"
#include "window.hh"


Mesh::Mesh(Vertex* vertices, 
           unsigned int vertexCount, 
           unsigned int* indices, 
           unsigned int indexCount):

    mVertices(vertices),
    mVertexCount(vertexCount),
    mIndices(indices),
    mIndexCount(indexCount) {

    init();

}


Mesh::Mesh(glm::vec2* vertices, 
           unsigned int vertexCount, 
           unsigned int* indices, 
           unsigned int indexCount):

    mVertices2(vertices),
    mVertexCount(vertexCount),
    mIndices(indices),
    mIndexCount(indexCount) {

    init();

}

Mesh::~Mesh() {

}

void Mesh::render(Camera* cam, const glm::mat4 &model)
{
    for (unsigned int i=0; i<mIndexCount; i+=3) {
        mContext->drawTriangle( mVertices[ mIndices[i]   ].transform(model), 
                                mVertices[ mIndices[i+1] ].transform(model), 
                                mVertices[ mIndices[i+2] ].transform(model) );
 
    } 

    // for (unsigned int i=0; i<mIndexCount; i+=3) {
    //     mContext->drawTriangle( mVertices[ mIndices[i]   ], 
    //                             mVertices[ mIndices[i+1] ], 
    //                             mVertices[ mIndices[i+2] ] );
 
    // } 



}


void Mesh::init() {
/*
    unsigned int xMin = 0xFFFFFFFF;
    unsigned int xMax = 0;
    unsigned int yMin = 0xFFFFFFFF;
    unsigned int yMax = 0;

    for (unsigned int i=0; i<mVertexCount; ++i) {
        
        if (mVertices[i].x < xMin) xMin = mVertices[i].x;
        if (mVertices[i].x > xMax) xMax = mVertices[i].x;
        if (mVertices[i].y < yMin) yMin = mVertices[i].y;
        if (mVertices[i].y > yMax) yMax = mVertices[i].y;
    }

    mContext = new RenderContext(xMin, yMin, xMax-xMin, yMax-yMin);
*/
    mContext = new RenderContext(0, 0, Window::W, Window::H);

/*
    std::vector<Vertex> normals;
    normals.reserve( mIndices->size() / 3 );

    // generate normals
    for (unsigned int i=0; i<mIndices->size(); i+=3) {
        
        // triangle
        Vertex vertex1 = (*mVertices)[ (*mIndices)[i  ] ].pos;
        Vertex vertex2 = (*mVertices)[ (*mIndices)[i+1] ].pos;
        Vertex vertex3 = (*mVertices)[ (*mIndices)[i+2] ].pos;
        
        // edge vectors
        Vertex u = vertex2 - vertex1;
        Vertex v = vertex3 - vertex1;
        
        normals.emplace_back( glm::cross(u, v) ); 
        
    }
*/

}


