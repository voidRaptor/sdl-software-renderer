#ifndef BITMAP_HH
#define BITMAP_HH

#include <SDL2/SDL.h>


/**
 * @brief Bitmap, "canvas" for drawing individual pixels
 * 
 */
class Bitmap {

public:
    Bitmap(int x, int y, int w, int h, unsigned int* pixels=nullptr);
    ~Bitmap();

    void render();
    void clear();


protected:
    SDL_Rect mDim;
    unsigned int* mPixels;

};


#endif